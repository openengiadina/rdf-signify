; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic rdf signify)
  #:use-module (schemantic rdf)
  #:use-module (schemantic fragment-graph)
  #:use-module ((schemantic ns) #:select (rdf xsd))
  #:use-module (schemantic rdf signify base32)

  #:use-module (rnrs bytevectors)

  #:use-module (ice-9 receive)
  #:use-module (ice-9 match)

  #:use-module (srfi srfi-171)

  #:use-module (sodium sign)
  #:use-module (sodium generichash)
  #:use-module (sodium base64)

  #:use-module (oop goops)

  #:export (generate-keypair
            sign
            verify))


(define-namespace rdf-signify "http://purl.org/rdf-signify#")

;; XSD base64Binary datatype

(define-class <xsd:base64Binary> (<literal>))

(define-method (literal-datatype (l <xsd:base64Binary>))
  (xsd "base64Binary"))

(define-method (literal-canonical (l <xsd:base64Binary>))
  (sodium-bin2base64 (literal-value l)))

(define-method (literal-lexical (l <xsd:base64Binary>))
  (literal-canonical l))

(define-method (write (l <xsd:base64Binary>) port)
  (format port "<xsd:base64Binary ~a>" (literal-canonical l)))

(define (make-base64binary-literal bv)
  (make <xsd:base64Binary> #:value bv))


;; encode/decode keys from IRIs

(define (public-key->iri pk)
  (make-iri (string-append "urn:ed25519:pk:" (base32-encode pk))))

(define (iri->public-key iri)
  (when (equal? (string-take (iri-value iri) 15)
                "urn:ed25519:pk:")
    (base32-decode (string-drop (iri-value iri) 15))))

(define (secret-key->iri sk)
  (make-iri (string-append "urn:ed25519:sk:" (base32-encode sk))))

(define (iri->secret-key iri)
  (when (equal? (string-take (iri-value iri) 15)
                "urn:ed25519:sk:")
    (base32-decode (string-drop (iri-value iri) 15))))

;; generate key pair

(define (generate-keypair)
  "Returns a newly generated public and secret key."
  (receive (pk sk)
      (crypto-sign-keypair)
    (values
     (public-key->iri pk)
     (secret-key->iri sk))))

;; sign

(define (blake2b-iri bv)
  (make-iri (string-append "urn:blake2b:"
                           (base32-encode (crypto-generichash bv)))))

(define* (sign #:key public-key secret-key message)
  (let* ((sk (iri->secret-key secret-key))
         (raw-message (iri-value message))
         (sig (crypto-sign-detatched #:secret-key sk #:message (string->utf8 raw-message)))
         (fg (make-fragment-graph (make-iri "urn:dummy"))))

    (graph-add fg (rdf "type") (rdf-signify "Signature"))
    (graph-add fg (rdf-signify "message") message)
    (graph-add fg (rdf "value") (make-base64binary-literal sig))
    (graph-add fg (rdf-signify "publicKey") public-key)

    ;; content-address the fragment graph
    (set! (fragment-graph-base-subject fg)
      (blake2b-iri (fragment-graph->csexp fg)))

    ;; return the fragment-graph
    fg))

;; (define pk
;;   (make-iri "urn:ed25519:pk:CL6VDCRFNQOSNWMQGCZ6WT3WLKITSLUZN4J6FWQYL4FNUHAXOVVQ"))

;; (define sk
;;   (make-iri "urn:ed25519:sk:CBWYRNIGAL4EPTB5OESYU2WS7FXS5FTUGHJ3CDG6NZV54L72L7ERF7KRRISWYHJG3GIDBM7LJ53FVEJZF2MW6E7C3IMF6CW2DQLXK2Y"))

;; (define message
;;   (make-iri "https://hello.com/"))

;; (define signature (sign #:public-key pk #:secret-key sk #:message message))

;; (verify signature)

(define (verify signature)
  (let ((messages (map triple-object
                       (graph-match (graph-query signature (rdf-signify "message")))))
        (public-keys (map triple-object
                          (graph-match (graph-query signature (rdf-signify "publicKey")))))
        (sig-values (map triple-object
                         (graph-match (graph-query signature (rdf "value"))))))
    (match (list messages public-keys sig-values)
      (((message) (public-key) (sig-value))
       (crypto-sign-verify-detatched #:signature (literal-value sig-value)
                                     #:message (string->utf8 (iri-value message))
                                     #:public-key (iri->public-key public-key)))

      (_ #f))))

