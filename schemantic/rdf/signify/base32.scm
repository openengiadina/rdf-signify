(define-module (schemantic rdf signify base32)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 vlist)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-60)
  #:use-module (srfi srfi-171)
  #:export (base32-encode
            base32-decode

            base32-alphabet-rfc4648))

;; The Base32 Data Encoding (RFC4648)

;; TODO padding

(define base32-alphabet-rfc4648
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567")

(define (make-encode-alphabet str)
  (alist->vhash
   (map
    cons
    (iota 32)
    (string->list str))))

(define (make-decode-alphabet str)
  (alist->vhash
   (map
    cons
    (string->list str)
    (iota 32))))

(define (quintet->alphabet-character q alphabet)
  (cdr
   (vhash-assv q alphabet)))

(define (alphabet-character->quintet char alphabet)
  (cdr (vhash-assv char alphabet)))

(define (pad-right lst n padding)
  (append
   lst
   (make-list (max 0 (- n (length lst))) padding)))

(define* (base32-tencode #:key (alphabet base32-alphabet-rfc4648))
  "Returns a transducer that returns the Base32 encoding of the concatenation of all bytevectors passed."
  (let ((alphabet* (make-encode-alphabet alphabet)))
    (compose
     ;; tranform group (40bit) into list of integers
     (tmap (lambda (bv) (bytevector->uint-list bv (endianness little) 1)))

     ;; flatten so that values are individual integers
     tflatten

     ;; get binary representation of integers
     (tmap (lambda (int) (integer->list int 8)))

     ;; take groups of 5bits
     tflatten (tsegment 5)

     ;; pad with zeroes if less than 5 bits available
     (tmap (lambda (group) (pad-right group 5 #f)))

     ;; encode 5bit group to an alphabet
     (tmap (lambda (quintet) (quintet->alphabet-character
                              (list->integer quintet)
                              alphabet*))))))

(define* (base32-encode bv #:key (alphabet base32-alphabet-rfc4648))
  "Encode bytevector to Base32 encoding."
  (list->string
   (port-transduce

    ;; create a Base32 transcoder
    (base32-tencode #:alphabet alphabet)

    ;; reduce into a list
    rcons

    ;; read 40bits at a time (5 bytes). Note that the amount of bytes doesn't
    ;; really matter for correctness, the transducer takes care of everything.
    ;; Maybe there is some performanc benefit...
    (lambda (port) (get-bytevector-n port 5))

    ;; open bytevector for reading
    (open-bytevector-input-port bv))))

;; See the Test vectors in the RFC (https://tools.ietf.org/html/rfc4648#section-10)
;; TODO put this in a unit test
;; (base32-encode (string->utf8 "f"))      ; MY
;; (base32-encode (string->utf8 "fo"))     ; MZXQ
;; (base32-encode (string->utf8 "foo"))    ; MZXW6
;; (base32-encode (string->utf8 "foob"))   ; MZXW6YQ
;; (base32-encode (string->utf8 "fooba"))  ; MZXW6YTB
;; (base32-encode (string->utf8 "foobar")) ; MZXW6YTBOI

(define (rbytevector)
  "Returns a reducer that accumulates integers into a bytevector"
  (let-values (((port get-bytevector) (open-bytevector-output-port)))
    (case-lambda
      ;; identity
      (() port)

      ;; completion
      ((port) (get-bytevector))

      ;; put bv on port and return the port
      ((port int) (begin (put-u8 port int) port)))))


(define* (base32-tdecode #:key (alphabet base32-alphabet-rfc4648))
  "Returns a transducer that decodes a Base32 encoded serie of characters and yields bytes as uints."
  (let ((alphabet* (make-decode-alphabet base32-alphabet-rfc4648)))
    (compose
     ;; decode alphabet-character
     (tmap (cut alphabet-character->quintet <> alphabet*))

     ;; expand to bit representation
     (tmap (cut integer->list <> 5))

     ;; flatten list of bits
     tflatten

     ;; group 8bits together
     (tsegment 8)

     ;; filter out groups with less than 8 bits
     (tfilter (lambda (group) (eqv? 8 (length group))))

     ;; tranform to integer
     (tmap list->integer))))

(define* (base32-decode str #:key (alphabet base32-alphabet-rfc4648))
  (list-transduce
   ;; create a transducer to decode the Base32 stream of characters
   (base32-tdecode #:alphabet alphabet)

   ;; create a new bytevector reducer
   (rbytevector)

   (string->list str)))

;; (utf8->string (base32-decode "MZXW6YTBOI")) ; "foobar"
