(hall-description
  (name "rdf-signify")
  (prefix "guile")
  (version "0.1")
  (author "pukkamustard")
  (copyright (2020))
  (synopsis "A RDF vocabulary for cryptographic signatures using Ed25519.")
  (description "")
  (home-page "https://gitlab.com/openengiadina/rdf-signify")
  (license gpl3+)
  (dependencies
   `(("guile-rdf" ,guile-rdf)
     ("guile-sodium" ,guile-sodium)))
  (files (libraries
          ((directory "schemantic"
                      ((directory "rdf" ((scheme-file "signify")
                                         (directory "signify"
                                                    ((scheme-file "base32")))))))))
         (tests ((directory "tests" ())))
         (programs ((directory "scripts" ())))
         (documentation
           ((org-file "README")
            (symlink "README" "README.org")
            (text-file "HACKING")
            (text-file "COPYING")
            (directory "doc" ((texi-file "rdf-signify")))))
         (infrastructure
           ((scheme-file "guix")
            (text-file ".gitignore")
            (scheme-file "hall")))))
