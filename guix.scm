(use-modules
  (guix packages)
  ((guix licenses) #:prefix license:)
  (guix download)
  (guix git-download)
  (guix build-system gnu)
  (gnu packages)
  (gnu packages crypto)
  (gnu packages autotools)
  (gnu packages guile)
  (gnu packages guile-xyz)
  (gnu packages pkg-config)
  (gnu packages ruby)
  (gnu packages texinfo))

(define-public guile-sodium
  (package
    (name "guile-sodium")
    (version "0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gitlab.com/openengiadina/guile-sodium")
               (commit "0344d63de6035f8e666085f88a49ffdcbaf0e7e5")))
        (file-name (git-file-name name version))
        (sha256 (base32 "00pr4j1biiklcr4q3x38fi45md866bql57aq2aima826jfkwws05"))))
    (build-system gnu-build-system)
    (arguments `())
    (native-inputs
      `(("autoconf" ,autoconf)
        ("automake" ,automake)
        ("pkg-config" ,pkg-config)
        ("texinfo" ,texinfo)))
    (inputs `(("guile" ,guile-3.0)))
    (propagated-inputs `(("libsodium" ,libsodium)))
    (synopsis "Guile bindings to libsodium.)")
    (description
      "This package provides bindings to libsodium which provides core cryptogrpahic primitives needed to build higher-level tools.")
    (home-page
      "https://gitlab.com/openengiadina/guile-sodium")
    (license license:gpl3+)))

(define-public guile-schemantic
  (package
    (name "guile-schemantic")
    (version "0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gitlab.com/openengiadina/guile-schemantic")
               (commit "286626137e5f58e9a06ca7c2e5b4aab9cf75f3a2")))
        (file-name (git-file-name name version))
        (sha256 (base32 "1n3ziqw97711i4g7cwq89rvbxw18l6wdz6dl19db62q0mahfw2s0"))))
    (build-system gnu-build-system)
    (arguments `())
    (native-inputs
      `(("autoconf" ,autoconf)
        ("automake" ,automake)
        ("pkg-config" ,pkg-config)
        ("texinfo" ,texinfo)))
    (inputs `(("guile" ,guile-3.0)))
    (propagated-inputs `(("guile-rdf" ,guile-rdf)))
    (synopsis "Guile library for the Semantic Web")
    (description
      "Guile Schemantic is a Guile library for the Semantic Web and implements the Resource Description Framework (RDF).")
    (home-page
      "https://gitlab.com/openengiadina/guile-schemantic")
    (license license:gpl3+)))

(package
  (name "guile-rdf-signify")
  (version "0.1")
  (source "./guile-rdf-signify-0.1.tar.gz")
  (build-system gnu-build-system)
  (arguments
   '(#:phases
     (modify-phases %standard-phases
       (add-after 'unpack 'run-hall
         (lambda _
           (setenv "HOME" "/tmp")   ; for ~/.hall
           (invoke "hall" "dist" "-x"))))))
  (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("pkg-config" ,pkg-config)
      ("texinfo" ,texinfo)
      ("guile-hall" ,guile-hall)
      ("asciidoctor" ,ruby-asciidoctor)))
  (inputs `(("guile" ,guile-3.0)))
  (propagated-inputs
   `(("guile-sodium" ,guile-sodium)
     ("guile-schemantic" ,guile-schemantic)))
  (synopsis
    "A RDF vocabulary for cryptographic signatures using Ed25519.")
  (description "")
  (home-page
    "https://gitlab.com/openengiadina/rdf-signify")
  (license license:gpl3+))

