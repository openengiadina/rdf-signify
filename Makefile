all: public/index.html public/rdf-signify.ttl

public/rdf-signify.ttl: rdf-signify.ttl
	mkdir -p public
	cp $< $@

public/index.html: rdf-signify.adoc rdf-signify.ttl
	mkdir -p public
	asciidoctor -o $@ -a webfonts! $<
